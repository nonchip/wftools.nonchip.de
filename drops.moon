import Https_json from require 'helpers'

class Drops
  new: (initAll=true)=>
    @endpoint='https://drops.warframestat.us/data/'
    if initAll
      @all_data, @all_apicode, @all_apistatus = @getAll!
  getAll: =>
    --this should be either called ONCE or split into the other endpoints since it's giant
    data=ngx.shared.cache\get 'alldrops'
    if data
      return data
    data,err=Https_json @endpoint..'all.json'
    if not data
      return nil, err
    ngx.shared.cache\set 'alldrops', data, 60*60
    data
