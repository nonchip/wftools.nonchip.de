Drops = require 'drops'
HDSL= require 'dysnomia.renderers.html5_dsl'

Tiers = {'Lith','Meso','Neo','Axi'}
for i,v in ipairs Tiers
  Tiers[v]=i

Relic_tier = (relic)-> string.match relic, "^(%a*) "

Format_relic = (relic)->
  tier,name,mod = string.match relic, "^(%a)[^ ]* (%a%d*) Relic(.*)$"
  if mod == ''
    mod = '    '
  else
    mod = ' '..string.sub mod,3,5
  "#{tier}-#{name}#{mod}"

template=do
  ipairs = ipairs -- weird proxy thing because closures are defined weird
  sort = table.sort
  string = string
  relictbl_cnt=0
  relictbl = HDSL =>
    relictbl_cnt+=1
    table class: 'fancytable', ->
      for headi,loc in ipairs @lr
        sort loc.drops, (a,b)->
          ca, cb = a.chance, b.chance
          if ca==cb
            Tiers[Relic_tier a.name] > Tiers[Relic_tier b.name]
          else
            ca > cb
        tr ->
          th id: 'loc'..headi..'t'..relictbl_cnt, colspan:3, scope: 'colgroup', loc.location
        tr ->
          th id: 'rel'..headi..'t'..relictbl_cnt, 'Relic'
          th id: 'cha'..headi..'t'..relictbl_cnt, 'Chance'
          th id: 'rot'..headi..'t'..relictbl_cnt, 'Rotation'
        for relic in *loc.drops
          tr ->
            td headers: 'loc'..headi..'t'..relictbl_cnt..' rel'..headi..'t'..relictbl_cnt, Format_relic relic.name
            td headers: 'loc'..headi..'t'..relictbl_cnt..' cha'..headi..'t'..relictbl_cnt, string.format '%6.2f %%', relic.chance
            td headers: 'loc'..headi..'t'..relictbl_cnt..' rot'..headi..'t'..relictbl_cnt, relic.rotation
  HDSL =>
    HTML5 ->
      head ->
        title 'Relic Farming'
        style '.fancytable{margin: 5px; vertical-align: top; border-collapse: collapse; border-spacing: 0; display: inline-block} .fancytable th{background: #ccc;} .fancytable td,th{border: 1px solid #999;}'
      body ->
        p 'This shows the currently best (by drop chance) endless missions for farming relics.'
        p 'The left table shows literally everything, while the middle table filters for "already seen" drops but ignores Sanctuary Onslaught and Derelict Survival, and the right one takes them into account while filtering.'
        p ->
          CDATA 'Source on '
          a href: 'https://gitlab.com/nonchip/wftools.nonchip.de', target: '_blank', 'GitLab'
          CDATA '.'
        for lr in *@lrs
          RAW relictbl\render {:lr}

->

  D = Drops!

  AllRelics = {}
  LocRelics = {}
  for planet,planet_tbl in pairs D.all_data.missionRewards
    for location,location_tbl in pairs planet_tbl
      continue if location_tbl.isEvent -- events don't count
      continue if location_tbl.rewards._id -- no rotation means no infinite mission so silly to farm
      for rotation,rotation_tbl in pairs location_tbl.rewards
        for reward in *rotation_tbl
          if string.match reward.itemName, " Relic"
              AllRelics[reward.itemName] or= 0
              AllRelics[reward.itemName] += 1
              LocRelics[planet..'/'..location] or= {}
              if not LocRelics[planet..'/'..location][reward.itemName] or LocRelics[planet..'/'..location][reward.itemName].chance < reward.chance
                LocRelics[planet..'/'..location][reward.itemName] = {chance:reward.chance, :rotation}

  FlatLocs = [{location:k, drops:v} for k,v in pairs LocRelics]
  table.sort FlatLocs, (a,b)->
    asum = 0
    bsum = 0
    for name,drop in pairs a.drops
      asum+=drop.chance
    for name,drop in pairs b.drops
      bsum+=drop.chance
    asum > bsum


  -- without sanctuary
  FinalLocRelicsU = {}
  for loc in *FlatLocs
    loctbl = {}
    for relic,relic_tbl in pairs loc.drops
      table.insert loctbl, {name:relic, chance: relic_tbl.chance, rotation: relic_tbl.rotation}
    if #loctbl > 0
      table.insert FinalLocRelicsU, {location:loc.location,drops:loctbl}

  -- without sanctuary
  FinalLocRelicsF = {}
  toSeeRelics={key,true for key,_ in pairs AllRelics}
  for loc in *FlatLocs
    break unless next toSeeRelics
    loctbl = {}
    for relic,relic_tbl in pairs loc.drops
      if not ( (loc.location\sub(1,9) == 'Sanctuary') or (loc.location\sub(1,8) == 'Derelict') )
        table.insert loctbl, {name:relic, chance: relic_tbl.chance, rotation: relic_tbl.rotation}
        toSeeRelics[relic] = nil
    if #loctbl > 0
      table.insert FinalLocRelicsF, {location:loc.location,drops:loctbl}

  -- with sanctuary
  FinalLocRelicsS = {}
  toSeeRelics={key,true for key,_ in pairs AllRelics}
  for loc in *FlatLocs
    break unless next toSeeRelics
    loctbl = {}
    for relic,relic_tbl in pairs loc.drops
      table.insert loctbl, {name:relic, chance: relic_tbl.chance, rotation: relic_tbl.rotation}
      toSeeRelics[relic] = nil
    if #loctbl > 0
      table.insert FinalLocRelicsS, {location:loc.location,drops:loctbl}

  template\render lrs:{FinalLocRelicsU,FinalLocRelicsF, FinalLocRelicsS}
