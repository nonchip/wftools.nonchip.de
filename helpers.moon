import request from require 'ssl.https'
import decode from require "cjson"

Https_json = (url)->
  res, code, headers, status = request url
  return nil, code, status unless code==200
  return (decode res), code, status


{:Https_json}
